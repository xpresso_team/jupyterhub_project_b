__all__ = ["ExploreText"]
__author__ = ["Srijan Sharma"]

import nltk
import pandas as pd
from nltk.util import ngrams
from nltk import word_tokenize
from collections import Counter
from nltk.corpus import stopwords
from xpresso.ai.core.logging.xpr_log import XprLogger

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()

class ExploreText():
    def __init__(self, data):
        nltk.download("punkt")
        nltk.download("stopwords")
        self.stoplist = set(stopwords.words('english'))
        self.data = data
        return

    def populate_text(self, remove_stopwords=True):
        """
            Creates a unigram, Bigram and Trigram Distribution of the
            Attribute data.
        """
        resp = dict()
        if isinstance(self.data, pd.Series):

            #creating corpus by joining text in the pandas series
            corpus = self.data.str.cat(sep=" ")
        elif isinstance(self.data,str):
            corpus = self.data
        else:
            logger.warning("Invalid Datatype provided to ExploreText."
                           "Possible input types are: Dataframe or string")
            return resp

        tokenizer = word_tokenize(corpus.lower())
        if remove_stopwords:
            tokenizer = [token for token in tokenizer if token not in self.stoplist]

        unigram = ngrams(tokenizer, 1)
        unigram_dist = Counter(unigram)

        bigrams = ngrams(tokenizer, 2)
        bigram_dist = Counter(bigrams)

        trigrams = ngrams(tokenizer, 3)
        trigram_dist = Counter(trigrams)

        resp["unigram"] = unigram_dist
        resp["bigram"] = bigram_dist
        resp["trigram"] = trigram_dist

        return resp